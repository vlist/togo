﻿// Note that this code has some hardwires related to data inputs. These hardwires are for demonstration purposes only and should be removed once data collection and model simulations are operational.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;
using IronPython.Hosting;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using System.Net.Mail;
using System.Net;
using DarkSkyApi;
using DarkSkyApi.Models;

namespace TogoModels
{
    public partial class Form1 : Form
    {
        static TraceSource trace;
        static System.Timers.Timer timerTogo;
        static System.Timers.Timer timerRerun;
        static System.Timers.Timer timer1stReminder;
        static System.Timers.Timer timer2ndReminder;
        static DateTime lastRun;
        static DateTime lastReRun;
        static DateTime lastReminder1st;
        static DateTime lastReminder2nd;

        public class ForecastSource
        {
            public string Name { get; set; }
            public int Id { get; set; }
            public int Priority { get; set; }
        }


        public Form1()
        {
            InitializeComponent();
            //For display on form:
            int runtime_hr = Convert.ToInt32(ConfigurationSettings.AppSettings["runtime_hr"].ToString());
            int runtime_min = Convert.ToInt32(ConfigurationSettings.AppSettings["runtime_min"].ToString());
            int rerun_hr = Convert.ToInt32(ConfigurationSettings.AppSettings["reRun_hr"].ToString());
            int rerun_min = Convert.ToInt32(ConfigurationSettings.AppSettings["reRun_min"].ToString());
            string dbName = ConfigurationSettings.AppSettings["databaseName"].ToString();
            label2.Text = "New run scheduled for: " + runtime_hr.ToString() + ":" + runtime_min.ToString("00");
            label3.Text = "Model Re-runs scheduled for: " + rerun_hr.ToString() + ":" + rerun_min.ToString("00");
            label4.Text = "saves to database: " + dbName;

            trace = new TraceSource("TogoModels");
            lastRun = DateTime.Now.AddMinutes(-2);
            lastReRun = DateTime.Now.AddMinutes(-2);
            lastReminder1st = DateTime.Now.AddMinutes(-2);
            lastReminder2nd = DateTime.Now.AddMinutes(-2);
            //Timer to run todays model once per day
            timerTogo = new System.Timers.Timer();
            int timerInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["timerInterval"].ToString());
            timerTogo.Interval = timerInterval;   // set to 30s
            timerTogo.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            timerTogo.Start();

            //Timer to run any past failed runs, once per day:
            timerRerun = new System.Timers.Timer();
            //int timerInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["timerInterval"].ToString());
            timerRerun.Interval = timerInterval;   // set to 30s
            timerRerun.Elapsed += new System.Timers.ElapsedEventHandler(timerRerun_Elapsed);
            timerRerun.Start();

            //Timer to send reminder email for run validations - 1st reminder
            timer1stReminder = new System.Timers.Timer();
            timer1stReminder.Interval = timerInterval;   // set to 30s
            timer1stReminder.Elapsed += new System.Timers.ElapsedEventHandler(timer1stReminder_Elapsed);
            timer1stReminder.Start();

            //Timer to send reminder email for run validations - 2nd reminder
            timer2ndReminder = new System.Timers.Timer();
            timer2ndReminder.Interval = timerInterval;   // set to 30s
            timer2ndReminder.Elapsed += new System.Timers.ElapsedEventHandler(timer2ndReminder_Elapsed);
            timer2ndReminder.Start();
        }

        static void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //timer elapsed so run model
                timerTogo.Stop();

                DateTime timeNow = DateTime.Now;
                int timeNow_Hour = timeNow.Hour;
                int timeNow_Minute = timeNow.Minute;
                double timeDiffMinutes = (timeNow - lastRun).TotalMinutes;
                //Get run time set in app.config file
                int runtime_hr = Convert.ToInt32(ConfigurationSettings.AppSettings["runtime_hr"].ToString());
                int runtime_min = Convert.ToInt32(ConfigurationSettings.AppSettings["runtime_min"].ToString());

                // run at noon every day
                //if (timeNow_Hour == 9 && (timeNow_Minute >= 38 && timeNow_Minute < 39))
                if (timeNow_Hour == runtime_hr && (timeNow_Minute >= runtime_min && timeNow_Minute < runtime_min +1) && timeDiffMinutes > 1.0)
                {
                    //update log file
                    trace.TraceEvent(TraceEventType.Information, 1, "Start procedure to run today's model, at "+DateTime.Now.ToString());
                    trace.Flush();
                    //update last run time:
                    lastRun = timeNow;

                    //setup new model run and parameters
                    int model_id = new int();
                    string damModelType = Convert.ToString(ConfigurationSettings.AppSettings["damModelType"].ToString());
                    string projectName = Convert.ToString(ConfigurationSettings.AppSettings["projectName"].ToString());
                  
                    using (Togo_data_Entities context = new Togo_data_Entities())
                    {

                        //Get an id number for this model run
                        var mod = from m in context.ModelRuns
                                  orderby m.id descending
                                  select m;

                        int int_test = new int();
                        int_test = mod.Count();

                        if (mod.Count() > 0)
                        {
                            model_id = mod.First().id + 1;
                        }
                        else
                        {
                            model_id = 1;
                        }

                        //get id number of this project from database
                        int projectId = new int();
                        var proj = from p in context.Projects
                                   where p.Name == projectName
                                   select p;

                        if (proj.Count() > 0)
                        {
                            projectId = proj.First().id;
                        }
                        else
                        {
                            //TODO:  should this result in model run error?
                            trace.TraceEvent(TraceEventType.Error, 1, "Could not find the project name in the database");
                            trace.Flush();
                        }

                        //Download darksky forecast data
                        DownloadForecastData(projectId);
                        

                        //create new model run in database
                        ModelRun model_new = new ModelRun();
                        model_new.id = model_id;
                        model_new.idProject = projectId;
                        model_new.StartTime = DateTime.Today;
                        model_new.EndTime = DateTime.Today.AddDays(3);
                        model_new.Status = "Ongoing";
                        model_new.RunStartTime = DateTime.Now;
                        model_new.RunEndTime = null;
                        model_new.idScenariosXMeta = null;
                        model_new.idScenariosXMetaOpt = null;

                        context.ModelRuns.Add(model_new);
                        context.SaveChanges();
                    }

                    RunVariables vars = new RunVariables();
                    vars.model_id = model_id;

                    //run model
                    RunStatus status = RunModel(vars);
                    if (status.status.ToLower() != "complete")
                    {
                        //add run status to DB if not complete (already added in run_model if complete):
                        using (Togo_data_Entities context = new Togo_data_Entities())
                        {
                            var q = from m in context.ModelRuns
                                    where m.id == model_id
                                    select m;
                            if (q.Count() > 0)
                            {
                                ModelRun run = q.First();
                                run.Status = status.status;
                                context.SaveChanges();
                            }
                        }
                    }

                    //Send alert
                    bool alertSuccess = SendAlert(model_id,status);
                }

                int timerInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["timerInterval"].ToString()); 
                timerTogo.Interval = timerInterval;
                timerTogo.Start(); 
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered in 'timer_Elapsed'");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
            }
        }

        static void timerRerun_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //timer elapsed so run model
                timerRerun.Stop();

                DateTime timeNow = DateTime.Now;
                int timeNow_Hour = timeNow.Hour;
                int timeNow_Minute = timeNow.Minute;
                double timeDiffMinutes = (timeNow - lastReRun).TotalMinutes;
                //Get run time set in app.config file
                int runtime_hr = Convert.ToInt32(ConfigurationSettings.AppSettings["reRun_hr"].ToString());
                int runtime_min = Convert.ToInt32(ConfigurationSettings.AppSettings["reRun_min"].ToString());
                string projectName = Convert.ToString(ConfigurationSettings.AppSettings["projectName"].ToString());

                if (timeNow_Hour == runtime_hr && (timeNow_Minute >= runtime_min && timeNow_Minute < runtime_min + 1) && timeDiffMinutes > 1.0)
                {
                    //update log file
                    trace.TraceEvent(TraceEventType.Information, 1, "Start procedure to run past failed model runs, at " + DateTime.Now.ToString());
                    trace.Flush();

                    //update last run time:
                    lastReRun = timeNow;

                    //Timer period for runs to re-run
                    int rerun_window = Convert.ToInt32(ConfigurationSettings.AppSettings["rerun_window"].ToString());
                    DateTime firstDate = DateTime.Now.Date.AddDays(-rerun_window);
                    int projectId = new int();
                    List<int> reruns = new List<int>();

                    //See if there are any days missing model runs (this is necessary because sometimes code fails before
                    //model run is added to database, so might just be missing rather than included as failed)
                    DateTime lastDate = DateTime.Now.Date.AddDays(1);
                    //Loop through each day and make sure there is a model run:
                    DateTime thisDate = firstDate;
                    try
                    {
                        using (Togo_data_Entities context = new Togo_data_Entities())
                        {
                            
                            int model_id = 1;
                            //Get an id number for this model run
                            var mod = from m in context.ModelRuns
                                      orderby m.id descending
                                      select m;

                            if (mod.Count() > 0)
                            {
                                model_id = mod.First().id + 1;
                            }

                            //get id number of this project from database
                            
                            var proj = from p in context.Projects
                                       where p.Name == projectName
                                       select p;

                            if (proj.Count() > 0)
                            {
                                projectId = proj.First().id;
                            }
                            else
                            {
                                //TODO:  should this result in model run error?
                                trace.TraceEvent(TraceEventType.Error, 1, "Could not find the project name in the database");
                                trace.Flush();
                            }

                            while (thisDate < lastDate)
                            {

                                DateTime endDate = thisDate.AddDays(1);
                                var dateQ = from m in context.ModelRuns
                                            where m.StartTime >= thisDate & m.StartTime < endDate
                                            select m;
                                //if no model runs for this date, add one to DB
                                if (dateQ.Count() == 0)
                                {
                                    //create new model run in database
                                    ModelRun model_new = new ModelRun();
                                    model_new.id = model_id;
                                    model_new.idProject = projectId;
                                    model_new.StartTime = thisDate;
                                    model_new.EndTime = thisDate.AddDays(3);
                                    model_new.Status = "Not Started";
                                    model_new.RunStartTime = null;
                                    model_new.RunEndTime = null;
                                    model_new.idScenariosXMeta = null;
                                    model_new.idScenariosXMetaOpt = null;

                                    context.ModelRuns.Add(model_new);
                                    context.SaveChanges();
                                    model_id = model_id + 1;

                                }
                                thisDate = endDate;
                            }
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while adding missing model runs to database");
                        trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                        trace.Flush();
                    }

                    string damModelType = Convert.ToString(ConfigurationSettings.AppSettings["damModelType"].ToString());
                    
                    try
                    {
                        using (Togo_data_Entities context = new Togo_data_Entities())
                        {


                            //get id number of this project from database
                            var proj = from p in context.Projects
                                       where p.Name == projectName
                                       select p;

                            if (proj.Count() > 0)
                            {
                                projectId = proj.First().id;
                            }
                            else
                            {
                                //TODO:  should this result in model run error?
                                trace.TraceEvent(TraceEventType.Error, 1, "Could not find the project name in the database");
                                trace.Flush();
                            }

                            //Search database for runs to re - run:
                            var mods = from m in context.ModelRuns
                                       where m.idProject == projectId & m.StartTime >= firstDate &
                                       m.Status.ToLower() != "complete"
                                       select m.id;

                            if (mods.Count() > 0)
                                reruns = mods.ToList();
                        } //end using
                    } //end try
                    catch (Exception ex)
                    {
                        trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while searching for failed model runs:");
                        trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                        trace.Flush();
                    }

                    foreach (int modId in reruns)
                    {

                        //modify run in database
                        try
                        {
                            using (Togo_data_Entities context = new Togo_data_Entities())
                            {
                                var modQ = from m in context.ModelRuns
                                           where m.id == modId
                                           select m;
                                ModelRun mod = modQ.First();
                                mod.Status = "Ongoing";
                                mod.RunStartTime = DateTime.Now;
                                mod.RunEndTime = null;
                                mod.idScenariosXMeta = null;
                                mod.idScenariosXMetaOpt = null;
                                context.SaveChanges();
                            } // end using


                            RunVariables vars = new RunVariables();
                            vars.model_id = modId;

                            //run model
                            RunStatus status = RunModel(vars);
                            //string statusStr = status.ToString();
                            if (status.status.ToLower() != "complete")
                            {
                                //add run status to DB if not complete (already added in run_model if complete):
                                using (Togo_data_Entities context = new Togo_data_Entities())
                                {
                                    var q = from m in context.ModelRuns
                                            where m.id == modId
                                            select m;
                                    if (q.Count() > 0)
                                    {
                                        ModelRun run = q.First();
                                        run.Status = status.status;
                                        context.SaveChanges();
                                    }
                                }
                            }

                            //Send alert
                            bool alertSuccess = SendAlert(modId, status);
                        } //end try
                        catch (Exception ex)
                        {
                            trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while re-running model run:");
                            trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                            trace.Flush();
                        }
                    } //end mod run loop
                } //end if 
                int timerInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["timerInterval"].ToString());
                timerRerun.Interval = timerInterval;
                timerRerun.Start();
            } //end try
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered in 'timerRerun_Elapsed'");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
            }
        }

        static void timer1stReminder_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //timer elapsed so run model
                timer1stReminder.Stop();

                int reminder1st_hr = Convert.ToInt32(ConfigurationSettings.AppSettings["valReminder1st_hour"].ToString());
                int reminder1st_min = Convert.ToInt32(ConfigurationSettings.AppSettings["valReminder1st_min"].ToString());
                string emailStr = ConfigurationSettings.AppSettings["reminderEmails1st"].ToString();

                DateTime timeNow = DateTime.Now;
                int timeNow_Hour = timeNow.Hour;
                int timeNow_Minute = timeNow.Minute;
                double timeDiffMinutes = (timeNow - lastReminder1st).TotalMinutes;

                if ((timeNow_Hour == reminder1st_hr) && (timeNow_Minute >= reminder1st_min && timeNow_Minute < reminder1st_min + 1) && timeDiffMinutes > 1.0)
                {
                    //update log file
                    trace.TraceEvent(TraceEventType.Information, 1, "Start procedure to send first reminder for unvalidated runs, at " + DateTime.Now.ToString());
                    trace.Flush();
                    //update last run time:
                    lastReminder1st = timeNow;

                    //check if runs in past 24 hours have not been validated:
                    DateTime start = timeNow.AddHours(-24);
                    try
                    {
                        using (Togo_data_Entities context = new Togo_data_Entities())
                        {
                            var query = from m in context.ModelRuns
                                        where m.StartTime >= start & m.Status.ToLower() == "complete"
                                        & m.idScenariosXMetaOpt == null
                                        select m;
                            if (query.Count()> 0)
                            {
                                //send email here!
                                List<ModelRun> runs = query.ToList();
                                foreach (ModelRun run in runs)
                                {
                                    //contents of message:
                                    string subject = "Reminder: Run needs to be validated";
                                    string body = "Run for time period " + run.StartTime.ToString("yyyy-MM-dd") + " to " + run.EndTime.ToString("yyyy-MM-dd")
                                                   + " is awaiting validation from dam operator";
                                    bool result = SendEmail(emailStr, subject, body);

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while sending 1st reminder for run valiation");
                        trace.Flush();
                    }
                }
                int timerInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["timerInterval"].ToString());
                timer1stReminder.Interval = timerInterval;
                timer1stReminder.Start();
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while sending 1st reminder for run valiation");
                trace.Flush();
            }
        }

        static void timer2ndReminder_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //timer elapsed so run model
                timer2ndReminder.Stop();

                int reminder2nd_hr = Convert.ToInt32(ConfigurationSettings.AppSettings["valReminder2nd_hour"].ToString());
                int reminder2nd_min = Convert.ToInt32(ConfigurationSettings.AppSettings["valReminder2nd_min"].ToString());
                int runtime_hr = Convert.ToInt32(ConfigurationSettings.AppSettings["reRun_hr"].ToString());
                int runtime_min = Convert.ToInt32(ConfigurationSettings.AppSettings["reRun_min"].ToString());
                string emailStr = ConfigurationSettings.AppSettings["reminderEmails2nd"].ToString();

                DateTime timeNow = DateTime.Now;
                int timeNow_Hour = timeNow.Hour;
                int timeNow_Minute = timeNow.Minute;
                double timeDiffMinutes = (timeNow - lastReminder2nd).TotalMinutes;

                if ((timeNow_Hour == reminder2nd_hr) && (timeNow_Minute >= reminder2nd_min && timeNow_Minute < reminder2nd_min + 1) && timeDiffMinutes > 1.0)
                {
                    //update log file
                    trace.TraceEvent(TraceEventType.Information, 1, "Start procedure to send second reminder for unvalidated runs, at " + DateTime.Now.ToString());
                    trace.Flush();
                    //update last run time:
                    lastReminder2nd = timeNow;
                    //check if runs in past 24 hours have not been validated:
                    DateTime start = timeNow.AddHours(-36);
                    try
                    {
                        using (Togo_data_Entities context = new Togo_data_Entities())
                        {
                            var query = from m in context.ModelRuns
                                        where m.StartTime >= start & m.Status.ToLower() == "complete"
                                        & m.idScenariosXMetaOpt == null
                                        select m;
                            if (query.Count() > 0)
                            {
                                //send email here!
                                List<ModelRun> runs = query.ToList();
                                foreach (ModelRun run in runs)
                                {
                                    //contents of message:
                                    string subject = "SECOND REMINDER: Run needs to be validated";
                                    string body = "Run for time period " + run.StartTime.ToString("yyyy-MM-dd") + " to " + run.EndTime.ToString("yyyy-MM-dd")
                                                   + " is awaiting validation from dam operator";
                                    bool result = SendEmail(emailStr, subject, body);

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while sending 2nd reminder for run valiation");
                        trace.Flush();
                    }
                }
                int timerInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["timerInterval"].ToString());
                timer2ndReminder.Interval = timerInterval;
                timer2ndReminder.Start();
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while sending 2nd reminder for run valiation");
                trace.Flush();
            }
        }

        public class RunVariables
        {
            public int model_id;
        }

        public class RunStatus
        {
            public string status { get; set; } //status to be added to DB
            public string detail { get; set; } //details to include in email
        }

        public static async void DownloadForecastData(int project_id)
        {
            //update log file
            trace.TraceEvent(TraceEventType.Information, 1, "Starting procedure to download forecast data at " + DateTime.Now.ToString());
            trace.Flush();
            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    var varRain = (from vv in context.Variables
                                   where vv.Name == "Precipitation"
                                   select vv).FirstOrDefault();

                    var varStaXRain = from vs in context.VariableXStations
                                      where vs.idVariable == varRain.id && vs.idProject == project_id
                                      select vs;

                    var source1 = (from s in context.ForecastedMetadatas
                                   where s.Source.ToLower() == "darksky"
                                   select s).FirstOrDefault();
                    int idDK = source1.id;

                    var fore = from f in context.ForecastedDatas
                               orderby f.id descending
                               select f;

                    int fore_id_last = 0;
                    if (fore.Count() > 0)
                    {
                        fore_id_last = fore.First().id + 1;
                    }
                    else
                    {
                        fore_id_last = 1;
                    }

                    foreach (VariableXStation v in varStaXRain)
                    {
                        var sta = (from ss in context.Stations
                                   where ss.id == v.idStation
                                   select ss).FirstOrDefault();

                        double xCoord = Convert.ToDouble(sta.xCoordinate);
                        double yCoord = Convert.ToDouble(sta.yCoordinate);

                        List<double> calc = new List<double>();

                        //Download dark sky data to database:
                        calc = await GetDarkSkyData(xCoord, yCoord);
                        DateTime date_use = DateTime.Now.Date;
                        DateTime fore_date_use = DateTime.Now;
                        foreach (double p in calc)
                        {
                            ForecastedData fore_new = new ForecastedData();
                            fore_new.id = fore_id_last;
                            fore_id_last = fore_id_last + 1;

                            date_use = date_use.AddDays(1);
                            fore_new.DateTime = date_use;

                            fore_new.idVariableXStation = v.id;
                            fore_new.ForecastedValue = p;
                            fore_new.ForecastedDateTime = fore_date_use;
                            fore_new.idSource = idDK;

                            context.ForecastedDatas.Add(fore_new);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered in 'DownloadForecastData':");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
            }
            

        }

        public static RunStatus RunModel(RunVariables args)
        {
            RunStatus status = new RunStatus();//run status to be returned
            status.status = "";
            status.detail = "";

            //update log file:
            trace.TraceEvent(TraceEventType.Information, 1, "Starting 'RunModel' procedure");
            trace.Flush();

            RunVariables vars = (RunVariables)args;
            int model_id = vars.model_id;
            DateTime SaveRunStartTime = new DateTime();
            DateTime SaveStartTime = new DateTime();

            //****************************************
            //            Upstream Model 
            //****************************************

            List<double> multipliers = new List<double> {0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00 };
            List<int> periods = new List<int> { 7, 14, 21, 56, 224 };//{1, 2, 7, 21, 56, 224};

            string TestMsg = "\n\nCe message fait partie d'un projet pilote soutenu par le Fonds mondial pour la prévention des catastrophes et de relèvement (GFDRR, de la Banque Mondiale) Code pour la résilience (CfR). \n Cette information est uniquement à des fins de démonstration!";
            string forecast_type = Convert.ToString(ConfigurationSettings.AppSettings["forecast_type"].ToString());
            int gap = Convert.ToInt32(ConfigurationSettings.AppSettings["gap"].ToString()); //21
            double power = Convert.ToDouble(ConfigurationSettings.AppSettings["power"].ToString());  //3.86742
            double IDWpower = Convert.ToDouble(ConfigurationSettings.AppSettings["IDWpower"].ToString());    //4.0
            double convert_msl_to_model_datum = Convert.ToDouble(ConfigurationSettings.AppSettings["convert_msl_to_model_datum"].ToString());   // -128 m

            // 1-day historical model
            double dam_coef_a_hist = Convert.ToDouble(ConfigurationSettings.AppSettings["dam_coef_a_hist"].ToString()); // 64.54384635;
            double dam_coef_b_hist = Convert.ToDouble(ConfigurationSettings.AppSettings["dam_coef_b_hist"].ToString()); // 0.477242095;
            double dam_coef_c_hist = Convert.ToDouble(ConfigurationSettings.AppSettings["dam_coef_c_hist"].ToString()); // 0.066574114;

            int nforecast_days = Convert.ToInt32(ConfigurationSettings.AppSettings["nforecast_days"].ToString());  //5

            int project_id = new int();
            List<int> forecast_number = new List<int>();
            DateTime dateUse = new DateTime();
            List<ForecastSource> foreSourceOrdered = new List<ForecastSource>();

            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    var varModel = (from tt in context.ModelRuns
                                    where tt.id == model_id
                                    select tt).FirstOrDefault();

                    project_id = varModel.idProject;
                    dateUse = varModel.StartTime;
                    
                    SaveRunStartTime = Convert.ToDateTime(varModel.RunStartTime);
                    SaveStartTime = varModel.StartTime;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Error finding model run in database");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Erreur";
                status.detail = "Une éxception était rencontré lors d'essai de trouvé la simulation dans la base de données.";
                return status;
            }

            for (int ii = 1; ii < nforecast_days + 1; ii++)
            {
                forecast_number.Add(ii);    
            }

            //forecast = [today's value, ....., last forecasted value]  - precipitation
            List<double> forecast_in = new List<double>(); 

            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    var varRain = (from vv in context.Variables
                                   where vv.Name == "Precipitation" 
                                   select vv).FirstOrDefault();

                    var varStaXRain = from vs in context.VariableXStations
                                      where vs.idVariable == varRain.id && vs.idProject == project_id
                                      select vs;

                    List<double> forecast_total = new List<double>();
                    forecast_total.Clear();
                    List<double> stationCount = new List<double>();
                    stationCount.Clear();
                    int loop = 0;

                    //Get priorities from config file:
                    string priorityStr = ConfigurationManager.AppSettings["ForecastSourcePriority"].ToString();
                    List<ForecastSource> foreSources = new List<ForecastSource>();
                    string[] temp = priorityStr.Split(',');
                    foreach (string t in temp)
                    {
                        ForecastSource thisSource = new ForecastSource();
                        string[] temp2 = t.Split(':');
                        thisSource.Name = temp2[0].Trim();
                        thisSource.Priority = Convert.ToInt32(temp2[1]);
                        var id = from f in context.ForecastedMetadatas
                                 where f.Source.Trim() == thisSource.Name
                                 select f.id;
                        if (id.Count() > 0)
                            thisSource.Id = id.First();
                        else
                            thisSource.Id = -1;
                        foreSources.Add(thisSource);
                    }
                    //order by priority:
                    foreSourceOrdered = foreSources.OrderBy(ii => ii.Priority).ToList();

                    foreach (VariableXStation v in varStaXRain)
                    {
                        var sta = (from ss in context.Stations
                                   where ss.id == v.idStation
                                   select ss).FirstOrDefault();


                        //Find latest available forecast for this station:
                        DateTime date_use = SaveStartTime;
                        List<double> calc2 = new List<double>();
                        var fc = from d in context.ForecastedDatas
                                 where d.idVariableXStation == v.id
                                 orderby d.DateTime descending
                                 select d;
                        DateTime station_end_date = date_use;
                        if (fc.Count() > 0)
                            station_end_date = fc.First().DateTime.Date;

                        while (date_use <= station_end_date)
                        {
                            DateTime date_use_end = date_use.AddDays(1);
                            var p2 = from d in context.ForecastedDatas
                                     where d.idVariableXStation == v.id &
                                     d.DateTime >= date_use & d.DateTime < date_use_end
                                     select d;
                            if (p2.Count() > 0)
                            {
                                //find highest priority forecast with latest date
                                foreach (ForecastSource ff in foreSourceOrdered)
                                {
                                    var q = from d in p2
                                            where d.idSource == ff.Id
                                            orderby d.ForecastedDateTime descending
                                            select d;
                                    if (q.Count() > 0)
                                    {
                                        calc2.Add(q.First().ForecastedValue);
                                        break;
                                    }
                                }
                            }
                            date_use = date_use.AddDays(1);
                        }
                        
                        if (loop == 0)
                        { 
                            foreach (double p in calc2)
                            {
                                forecast_total.Add(p);
                                stationCount.Add(1);
                            }
                            loop = 1;
                        }
                        else
                        {
                            for (int pp = 0; pp < calc2.Count(); pp++)
                            {
                                if (pp < forecast_total.Count())
                                {
                                    forecast_total[pp] = forecast_total[pp] + calc2[pp];
                                    stationCount[pp] = stationCount[pp] + 1;
                                }
                                else
                                {
                                    forecast_total.Add(pp);
                                    stationCount.Add(1);
                                }
                            }
                        };

                       
                    }
                    //context.SaveChanges();
                    int i2 = 0;
                    foreach (double p in forecast_total)
                    {
                        forecast_in.Add(p /stationCount[i2]);
                        i2 = i2 + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.TraceEvent(TraceEventType.Critical, 1, "Error getting forecast data");
                trace.Flush();
                status.status = "Erreur d'affichage de données";
                status.detail = "Une éxception était rencontré lors de l'obtien de données de prévision.";
                return status;
            }


            // ****** FLOW AND RAIN DATA ********

            List<double> flow_in = new List<double>();
            List<DateTime> time_flow_in = new List<DateTime>();
            List<double> rain_in = new List<double>();
            dateUse = SaveStartTime;
            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    //query for project 
                    var varModel = (from tt in context.ModelRuns
                                    where tt.id == model_id
                                    select tt).FirstOrDefault();

                    //flow - get from database
                    var varFlow = (from vv in context.Variables
                                   where vv.Name == "Inflow"  
                                   select vv).FirstOrDefault();

                    //take the flow corresponding to the project 
                    var varStaXFlow = (from vs in context.VariableXStations
                                       where vs.idVariable == varFlow.id && vs.idProject == project_id
                                       select vs).FirstOrDefault();

                    // pull flow series and corresponding times
                    var query = from dd in context.DataValues
                                     where dd.idVariableXStation == varStaXFlow.id && dd.DateTime < dateUse
                                     orderby dd.DateTime ascending      // [oldest value, ......, newest value] 
                                     select dd;
                    List<DataValue> dat = query.ToList();
                    var distDates = dat.GroupBy(d => d.DateTime).Select(group => group.First());
                    foreach (DataValue v in distDates)
                    {
                        flow_in.Add(Convert.ToDouble(v.Value));
                        time_flow_in.Add(Convert.ToDateTime(v.DateTime));
                    }

                    //Get precipitation data from database

                    var varRain = (from vv in context.Variables
                                   where vv.Name == "Precipitation" 
                                   select vv).FirstOrDefault();

                    var varStaXRain = from vs in context.VariableXStations
                                      where vs.idVariable == varRain.id && vs.idProject == project_id
                                      select vs;

                    foreach (DateTime t in time_flow_in)
                    {
                        // this is the precipitation of the PREVIOUS day, respect to flow: flow[i] is the flow of the day, while rain[i] is the precipitation of the previous day
                        DateTime time_use = t.Date; 

                        double rain_total = 0.0;
                        double rain_average = 0.0;

                        foreach (VariableXStation v in varStaXRain)
                        {

                            double rain_add = new double();

                            DateTime ub = time_use.AddDays(1);

                            var rainUse = from r in context.DataValues
                                          where r.idVariableXStation == v.id && r.DateTime >= time_use && r.DateTime < ub 
                                          orderby r.DateTime descending // most recent value first
                                          select r;

                            if (rainUse.Count() > 0)
                            {
                                rain_add = Convert.ToDouble(rainUse.First().Value);
                            }
                            else       // if query returns nothing, then must take from subsequent station or forecasted value
                            {
                                status.detail = "Les données de pluie substitué pour un ou plusieurs stations  (données non disponible)";  //substitute rainfall data used
                                bool rain_add_found = false;
                                var sub = from s in context.StaSubOrders
                                          where s.idVariableXStation == v.id
                                          orderby s.Priority ascending  // start with priority 1 substitution station
                                          select s;

                                foreach (StaSubOrder ss in sub)
                                {
                                    var rainUseSub = from rs in context.DataValues
                                                     where rs.idVariableXStation == ss.idStationSub && rs.DateTime >= time_use && rs.DateTime < ub
                                                     orderby rs.DateTime descending //most recent value first
                                                     select rs;
                                    if (rainUseSub.Count() > 0)
                                    {
                                        rain_add = Convert.ToDouble(rainUseSub.First().Value);
                                        rain_add_found = true;
                                        break;
                                        //use available value, if missing then go to next station in priority list
                                    }
                                }
                                if (rain_add_found != true)
                                {
                                    status.detail = "les données de prévision de pluie utilisé pour un ou plusieurs stations";
                                    bool rain_sub_found = false;
                                    var foreUseSub = from fs in context.ForecastedDatas
                                                     where fs.idVariableXStation == v.id && fs.DateTime >= time_use && fs.DateTime < ub
                                                     //orderby fs.ForecastedDateTime descending //most recent FORECASTED value first
                                                     select fs;

                                    if (foreUseSub.Count() > 0)
                                    {
                                        //find highest priority forecast with latest date
                                        foreach (ForecastSource ff in foreSourceOrdered)
                                        {
                                            var q = from d in foreUseSub
                                                    where d.idSource == ff.Id
                                                    orderby d.ForecastedDateTime descending
                                                    select d;
                                            if (q.Count() > 0)
                                            {
                                                rain_add = q.First().ForecastedValue;
                                                rain_sub_found = true;
                                                break;
                                            }
                                        }
                                    }

                                    if ( !rain_sub_found)
                                    {
                                        trace.TraceEvent(TraceEventType.Error, 1, "Could not find a substitute rain record");
                                        status.detail = "Données de substitution pas trouvé pour un ou plusieurs stations";
                                    }
                                }

                            }
                            rain_total = rain_total + rain_add;

                        }
                        rain_average = rain_total / varStaXRain.Count();
                        rain_in.Add(rain_average);
                    }

                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while getting rain and flow data:");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Erreur d'affichage de données";
                status.detail = "Le modèle à rencontré une éxception dans l'obtien de données pluvio et débit."; 
                return status;
            }

            //---------------------------------------------------

            // loop through forecast number to run upstream model equations and get forecasted flows
            List<double> foreflow = new List<double>();
            try
            {
                
                foreach (int ii in forecast_number)
                {
                    List<double> target = creatarget(forecast_in, ii, rain_in, periods);    // returns list of period rain averages
                    trace.TraceEvent(TraceEventType.Information, 1, "Day = " + ii + ", precip period values = " + string.Join(",", target));
                    trace.Flush();
                    double foreflow_single = calcIDW(target, flow_in, rain_in, periods, gap, multipliers, power, IDWpower); // returns single flow corresponding forecast_number
                    foreflow.Add(foreflow_single);  // create a list of the forecasted flows
                    trace.TraceEvent(TraceEventType.Information, 1, "Flow predicted = " + foreflow_single.ToString());
                    trace.Flush();
                
                    using (Togo_data_Entities context = new Togo_data_Entities())
                    {
                        var val = from v in context.ModelValues
                                  orderby v.id descending
                                  select v;

                        // store the forecasted inflows to ModelValues for later reference
                        ModelValue modVal_add = new ModelValue();

                        if (val.Count() > 0)
                        {
                            modVal_add.id = val.First().id + 1;
                        }
                        else
                        {
                            modVal_add.id = 1;
                        }

                        //flow
                        var varFlow = (from vv in context.Variables
                                       where vv.Name == "Inflow"
                                       select vv).FirstOrDefault();

                        //take the flow corresponding to the project 
                        var varStaXFlow = (from vs in context.VariableXStations
                                           where vs.idVariable == varFlow.id && vs.idProject == project_id
                                           select vs).FirstOrDefault();

                        modVal_add.idModelRun = model_id;
                        modVal_add.idVariableXStation = varStaXFlow.id;
                        modVal_add.StartTime = SaveRunStartTime;
                        modVal_add.Value = foreflow_single;
                        modVal_add.Day = ii-1;
                        modVal_add.Optimized = false;

                        context.ModelValues.Add(modVal_add);
                        context.SaveChanges();
                    }
                }

            }
                catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while running upstream model:");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Erreur du modèle de l'amont";
                status.detail = "Une éxception était rencontré lors du simulation du modèle de l'amont";
                return status;
            }

            //add inflow DATA from "yesterday" and the day before:
            for (int ii = -2; ii < 0; ii++)
            {
                try
                {
                    using (Togo_data_Entities context = new Togo_data_Entities())
                    {
                        var val = from v in context.ModelValues
                                  orderby v.id descending
                                  select v;

                        // store inflow from two previous days, although this is not used
                        ModelValue modVal_add = new ModelValue();

                        if (val.Count() > 0)
                        {
                            modVal_add.id = val.First().id + 1;
                        }
                        else
                        {
                            modVal_add.id = 1;
                        }

                        //flow
                        var varFlow = (from vv in context.Variables
                                       where vv.Name == "Inflow"
                                       select vv).FirstOrDefault();

                        //take the flow corresponding to the project 
                        var varStaXFlow = (from vs in context.VariableXStations
                                           where vs.idVariable == varFlow.id && vs.idProject == project_id
                                           select vs).FirstOrDefault();

                        DateTime dateUse_LB = dateUse.AddDays(ii);
                        DateTime dateUse_UB = dateUse.AddDays(ii + 1);

                        var data = from f in context.DataValues
                                   where f.idVariableXStation == varStaXFlow.id && f.DateTime > dateUse_LB && f.DateTime < dateUse_UB
                                   select f;

                        if (data.Count() > 0)
                        {
                            modVal_add.idModelRun = model_id;
                            modVal_add.idVariableXStation = varStaXFlow.id;
                            modVal_add.StartTime = SaveRunStartTime;
                            modVal_add.Value = data.First().Value;
                            modVal_add.Day = ii;
                            modVal_add.Optimized = false;

                            context.ModelValues.Add(modVal_add);
                            context.SaveChanges();
                        }
                        else
                        {
                            //if can't find inflow in db from the appropriate day then does nothing 
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while finding inflow");
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    trace.Flush();
                    status.status = "Erreur";
                    status.detail = "Une éxception était rencontré lors de l'obtien de données débit d'amont.";
                    return status;
                }
            }

            //****************************************
            //              Dam Model
            //****************************************
            // use predicted flow rates and rating curve to get downstream flow rate
            List<area_elev_val> area_elev = new List<area_elev_val>();

            List<double> elev_list = new List<double>();
            List<double> area_list = new List<double>();

            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    var varLevel = (from vv in context.Variables
                                    where vv.Name == "Water Level" 
                                    select vv).FirstOrDefault();

                    var varStaXFlow = (from vs in context.VariableXStations
                                       where vs.idVariable == varLevel.id && vs.idProject == project_id
                                       select vs).FirstOrDefault();
                    
                    var varAreaElev = from ae in context.DamCurves
                                      where ae.idStation == varStaXFlow.idStation
                                      select ae;

                    foreach (DamCurve dc in varAreaElev)
                    {
                        area_elev_val area_elev_item = new area_elev_val();
                        area_elev_item.elev = dc.Elevation;
                        area_elev_item.area = dc.Area;
                        area_elev.Add(area_elev_item);
                    }

                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while getting level and area data from database");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Erreur d'affichage de données";
                status.detail = "Une éxception était rencontré lors de l'obtien de données niveau d'eau et l'aérea.";
                return status;
            }
            
            //---------------------------------------------------------------
            double levelUse = new double(); 
            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    var varLevel = (from vv in context.Variables
                                    where vv.Name == "Water Level" 
                                    select vv).FirstOrDefault();

                    var varStaXFlow = (from vs in context.VariableXStations
                                       where vs.idVariable == varLevel.id && vs.idProject == project_id
                                       select vs).FirstOrDefault();

                    DateTime lb = dateUse.AddDays(-1);  // query for previous day's water level
                    int levelSta_save = new int();
                    levelSta_save = varStaXFlow.id;

                    var levelQuery = from l in context.DataValues
                                     where l.idVariableXStation == varStaXFlow.id && l.DateTime >= lb && l.DateTime < dateUse
                                     orderby l.DateTime descending   // should only be one value but if there is more than one value, just take most recent. 
                                     select l;

                    if (levelQuery.Count() > 0)
                    {
                        // if there is a water level, then set level use as the water level
                        levelUse = Convert.ToDouble(levelQuery.First().Value);
                    }
                    else
                    {
                        var levelAll = (from ll in context.DataValues
                                       where ll.idVariableXStation == varStaXFlow.id
                                       orderby ll.DateTime descending
                                       select ll).ToList();
                        
                        // if no value for the selected day, then take the oldest value. do not save to DB
                        levelUse = Convert.ToDouble(levelAll.First().Value);  // take the oldest value  
                    }

                    // save water level to ModelValues for later reference
                    var val = from v in context.ModelValues
                              orderby v.id descending
                              select v;

                    ModelValue modVal_add = new ModelValue();
                    if (val.Count() > 0)
                    {
                        modVal_add.id = val.First().id + 1;
                    }
                    else
                    {
                        modVal_add.id = 1;
                    }
                    modVal_add.idVariableXStation = levelSta_save;
                    modVal_add.StartTime = SaveRunStartTime;
                    modVal_add.Value = levelUse;
                    modVal_add.Day = -1;    // yesterday's water level
                    modVal_add.idModelRun = model_id;
                    modVal_add.Optimized = false;

                    context.ModelValues.Add(modVal_add);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while getting level data");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Manque de données";
                status.detail = "Une éxception était rencontré lors de l'obtien de données niveau d'eau.";
                return status;

                //using (Togo_data_Entities context = new Togo_data_Entities())
                //{
                //    var mod = (from m in context.ModelRuns
                //               where m.id == model_id
                //               select m).FirstOrDefault();

                //    mod.Status = "Lack of Data";
                //    context.SaveChanges();
                //    return;
                //}
            }
            List<double> Qout_list = new List<double>();

            double level = levelUse + convert_msl_to_model_datum;

            if ((level > 20.0) | (level < 0.0))
            {
                //using (Togo_data_Entities context = new Togo_data_Entities())
                //{
                //    var mod = (from m in context.ModelRuns
                //               where m.id == model_id
                //               select m).FirstOrDefault();

                //    mod.Status = "Input Error";
                //    context.SaveChanges();
                //    return;
                //}
                status.status = "Erreur d'affichage de données";
                status.detail = "Niveau hors du limite.";
                return status;
            }
            foreach (double q in foreflow)
            {
                try
                {
                    double Qin_use = q;
                    double Qout_new = calcdamout(dam_coef_a_hist, dam_coef_b_hist, dam_coef_c_hist, Qin_use, level);

                    Qout_list.Add(Qout_new);

                    level = updatelevel(Qin_use, Qout_new, level, area_elev);   // adjust level_use for the subsequent iterations
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered while calculating outflow");
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    trace.Flush();
                    status.status = "Erreur";
                    status.detail = "Une éxception était rencontré lors du calcul de débit";
                    return status;
                }
            }

            //****************************************
            //         Downstream Model
            //****************************************
            
            List<sc_val> scenarios = new List<sc_val>();
            
            double flow_t2 = new double(); 
            double flow_t1 = new double();
            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    var varSc = from ss in context.Scenarios
                                select ss;

                    foreach (Scenario s in varSc)
                    {
                        sc_val scItem = new sc_val();
                        scItem.num = s.Number;
                        scItem.flow1 = s.Flow1;
                        scItem.flow2 = s.Flow2;
                        scItem.flow3 = s.Flow3;
                        scItem.flow4 = s.Flow4;
                        scItem.flow5 = s.Flow5;

                        scenarios.Add(scItem);
                    }
                }
                using (Togo_data_Entities context2 = new Togo_data_Entities())
                {
                    var varOut = (from vv in context2.Variables
                                    where vv.Name == "Outflow"  
                                    select vv).FirstOrDefault();

                    var varStaXFlow = (from vs in context2.VariableXStations
                                       where vs.idVariable == varOut.id && vs.idProject == project_id
                                       select vs).FirstOrDefault();

                    // query for dam outflow from two days ago
                    DateTime lb = dateUse.AddDays(-1);
                    DateTime lb2 = dateUse.AddDays(-2);

                    var varFlow2 = from f in context2.DataValues
                                   where f.idVariableXStation == varStaXFlow.id & f.DateTime >= lb2 & f.DateTime < lb
                                   orderby f.DateTime descending   // should only be one value, but if more than one than just take most recent
                                   select f;

                    if (varFlow2.Count() > 0)
                    {
                        flow_t2 = Convert.ToDouble(varFlow2.First().Value);
                    }
                    else
                    {
                        var FlowAll = (from fl in context2.DataValues
                                      where fl.idVariableXStation == varStaXFlow.id
                                      orderby fl.DateTime descending
                                      select fl).ToList();


                        flow_t2 = Convert.ToDouble(FlowAll.Last().Value);  // HARDWIRE for demonstration only until flow data is actively added to database - if no value then take the oldest outflow value

                    }
                }
                using (Togo_data_Entities context3 = new Togo_data_Entities())
                {
                    // query database for yesterday's dam outflow
                    var varOut = (from vv in context3.Variables
                                  where vv.Name == "Outflow" 
                                  select vv).FirstOrDefault();

                    var varStaXFlow = (from vs in context3.VariableXStations
                                       where vs.idVariable == varOut.id && vs.idProject == project_id
                                       select vs).FirstOrDefault();

                    // query for flow from two days ago
                    DateTime lb = dateUse.AddDays(-1);
                    
                    var varFlow = from f in context3.DataValues
                                   where f.idVariableXStation == varStaXFlow.id & f.DateTime >= lb & f.DateTime < dateUse
                                  orderby f.DateTime descending // should only be one value, but if more than one than just take most recent
                                  select f;

                    if (varFlow.Count() > 0)
                    {
                        flow_t1 = Convert.ToDouble(varFlow.First().Value);
                    }
                    else
                    {
                        var FlowAll = (from fl in context3.DataValues
                                      where fl.idVariableXStation == varStaXFlow.id
                                      orderby fl.DateTime descending
                                      select fl).ToList();

                        flow_t1 = Convert.ToDouble(FlowAll.Last().Value);  // HARDWIRE for demonstration only until flow data is actively added to database - take the oldest value

                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception encountered getting data for downstream model"); 
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Erreur d'affichage de données";
                status.detail = "Une éxception était rencontré lors de l'obtien de données pour le modèle de l'aval";
                return status;
                //using (Togo_data_Entities context = new Togo_data_Entities())
                //{
                //    var mod = (from m in context.ModelRuns
                //               where m.id == model_id
                //               select m).FirstOrDefault();

                //    mod.Status = "Input Error";
                //    context.SaveChanges();
                //    return;
                //}
            }

            List<double> target_down = new List<double>();
            target_down.Add(flow_t2);
            target_down.Add(flow_t1);

            for (int qq = 0; qq < 3; qq++)  // 
            {
                target_down.Add(Qout_list[qq]); // target of 5 outflows. past two (yesterday and day before) from observed, and 3 (today, tomorrow, next day) from dam routing
            }

            for (int qq = 0; qq < target_down.Count(); qq++)
            {
                try
                {
                    using (Togo_data_Entities context = new Togo_data_Entities())
                    {
                        var varOut = (from vv in context.Variables
                                      where vv.Name == "Outflow"
                                      select vv).FirstOrDefault();

                        var varStaXFlow = (from vs in context.VariableXStations
                                           where vs.idVariable == varOut.id && vs.idProject == project_id
                                           select vs).FirstOrDefault();

                        var predAll = from p in context.ModelValues
                                      orderby p.id descending
                                      select p;

                        ModelValue modVal_add = new ModelValue();

                        if (predAll.Count() > 0)
                        {
                            modVal_add.id = predAll.First().id + 1;
                        }
                        else
                        {
                            modVal_add.id = 1;
                        }

                        modVal_add.idVariableXStation = varStaXFlow.id;
                        modVal_add.StartTime = SaveRunStartTime;
                        modVal_add.Value = target_down[qq];
                        modVal_add.Day = (-2 + qq);
                        modVal_add.idModelRun = model_id;
                        modVal_add.Optimized = false;

                        context.ModelValues.Add(modVal_add);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, "Exception while adding model values to dataase");
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    trace.Flush();
                    //status.status = "Input Error";
                    //status.detail = "";
                    //return status;
                    //using (Togo_data_Entities context = new Togo_data_Entities())
                    //{
                    //    var mod = (from m in context.ModelRuns
                    //               where m.id == model_id
                    //               select m).FirstOrDefault();

                    //    mod.Status = "Input Error";
                    //    context.SaveChanges();
                    //    return;
                    //}
                }
            }

            sc_val result_down = new sc_val();
            try
            {
                result_down = findclosest(target_down, scenarios);   // returns sc_val with scenario number, not ID
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Erreur";
                status.detail = "Une éxception était rencontré lors de l'obtien d'un número de scénario";
                return status; 
            }
            int idMeta = new int();

            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    var varSc = (from ss in context.Scenarios
                                where ss.Number == result_down.num  
                                select ss).FirstOrDefault();

                    var varScMeta = (from sm in context.ScenariosXMetas
                                     where sm.idScenarios == varSc.id
                                     select sm).FirstOrDefault();

                    var mod = (from m in context.ModelRuns
                               where m.id == model_id
                               select m).FirstOrDefault();

                    idMeta = varScMeta.idMetascenario;
                    mod.idScenariosXMeta = varScMeta.id;    // put result (scenario) to database
                    mod.Status = "Complete";
                    mod.RunStartTime = SaveRunStartTime;
                    mod.RunEndTime = DateTime.Now;
                    context.SaveChanges();
                    status.status = "Complete";
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                status.status = "Erreur";
                status.detail = "Une éxception était rencontré.";
                return status;
            }

            
            return status;
        }

        public static bool SendAlert(int modelId, RunStatus status)
        {
            string TestMsg = "\n\nCe message fait partie d'un projet pilote soutenu par le Fonds mondial pour la prévention des catastrophes et de relèvement (GFDRR, de la Banque Mondiale) Code pour la résilience (CfR). \n Cette information est uniquement à des fins de démonstration!";
            bool success = true;
            //send email alert
            try
            {
                using (Togo_data_Entities context = new Togo_data_Entities())
                {
                    //find model run
                    var mod = (from m in context.ModelRuns
                               where m.id == modelId
                               select m).FirstOrDefault();

                    if (status.status.ToLower() == "complete")
                    {
                        string detail = "";
                        if (status.detail != "")
                        {
                            detail = "*** " + status.detail + " ***\n";
                        }
                        int idMeta = mod.ScenariosXMeta.idMetascenario;

                        var mXa = from ma in context.MetaXActions
                                  where ma.idMetascenarios == idMeta
                                  select ma;

                        //var meta = (from m in context.Metascenarios
                        //            where m.id == idMeta
                        //            select m).FirstOrDefault();

                        var metaEng = (from mm in context.Metascenarios
                                       where mm.id == idMeta
                                       select mm).FirstOrDefault();

                        var meta = (from mm in context.MetascenariosFrenches
                                    where mm.id == metaEng.idMetascenariosFrench
                                    select mm).FirstOrDefault();

                        foreach (MetaXAction mm in mXa)
                        {
                            var user = (from u in context.Users
                                        where u.id == mm.idUsers
                                        select u).FirstOrDefault();

                            var act = (from a in context.Actions
                                       where a.id == mm.idAction
                                       select a).FirstOrDefault();

                            string action = act.Type;
                            bool sendSuccess = false;

                            if (action.ToLower().Substring(0, 5) == "email")
                            {
                                string[] temp;
                                string emailUse = user.Email;
                                temp = emailUse.Split(';');

                                foreach (string s in temp)
                                {
                                    string comAddress = s;
                                    string msg0 = "Ceci est un message automatique en ce qui concerne le risque actuel d'inondation dans Mono bassin de la rivière, sur la base d'un outil d'aide à la décision de prototype.\n\n";
                                    string msg = "La période de prévision de " + mod.StartTime.ToString("dd MMM yyyy") + " au " + mod.EndTime.ToString("dd MMM yyyy") + " prédire les conditions suivantes:\n" + meta.Risk + " Risque. " + meta.Description;

                                    msg = msg0 + msg + detail + TestMsg;
                                    sendSuccess = SendEmail(comAddress, "Simulation automatique " + mod.StartTime.ToString("dd MMM yyyy") + " au " + mod.EndTime.ToString("dd MMM yyyy") + ": " + meta.Risk + " Risque", msg);
                                    if (!sendSuccess)
                                    {
                                        trace.TraceEvent(TraceEventType.Error, 1, "Failed to send communication through " + action + " to " + comAddress);
                                        trace.Flush();
                                        success = false;
                                    }
                                }
                            }
                            else
                            {
                                //action type not setup
                                trace.TraceEvent(TraceEventType.Error, 1, "Failed to send communication - either action format is incorrect or communication type is not setup.  Message not sent");
                                trace.Flush();
                                success = false;
                                return success;
                            }
                        }
                    }
                    else
                    {
                        List<string> addresses = new List<string> { "jtroychock@envmodeling.com", "kpangon.pascal@yahoo.fr", "viainonc@yahoo.fr", "lareromain@yahoo.fr" }; //TODO add addresses
                        string subject = "Simulation automatique " + mod.StartTime.ToString("dd MMM yyyy") + " au " + mod.EndTime.ToString("dd MMM yyyy") + " n'est pas reussi de completer";
                        string msg0 = "Ceci est un message automatique en ce qui concerne le risque actuel d'inondation dans Mono bassin de la rivière, sur la base d'un outil d'aide à la décision de prototype.\n\n";
                        string msg = "La période de prévision de " + mod.StartTime.ToString("dd MMM yyyy") + " au " + mod.EndTime.ToString("dd MMM yyyy") + " n'est pas reussi de completer, pour ce raisin:\n";

                        msg = msg0 + msg + status.status +":\n" + status.detail + "/n" + TestMsg;
                        bool sendSuccess = false;
                        foreach (string address in addresses)
                        {
                            sendSuccess = SendEmail(address, subject, msg);
                            if (!sendSuccess)
                            {
                                trace.TraceEvent(TraceEventType.Error, 1, "Failed to send communication through email to " + address);
                                trace.Flush();
                                success = false;
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                success = false;
                return success;
            }
            return success;
        }

        public static async Task<List<double>> GetDarkSkyData(double lat, double lon)
        {


            try
            {
                Forecast result = new Forecast();
                var client = new DarkSkyService("7d8c90670ef914c9b96344a737f790a4");
                result = await client.GetWeatherDataAsync(lat, lon);
                //results = await GetForecastAsync(lat, lon, "7d8c90670ef914c9b96344a737f790a4");

                List<double> dailyPrecips = new List<double>();
                //loop through days
                for (int i = 0; i < result.Daily.Days.Count(); i++)
                {
                    double intensity = result.Daily.Days[i].PrecipitationIntensity;
                    //calculate amount for 24 hours
                    double dailyPrecip = intensity * 24.0;
                    dailyPrecips.Add(dailyPrecip);

                }



                //trace.TraceEvent(TraceEventType.Verbose, 1, "Reading data for station code = " + s.Code);
                //trace.Flush();
                return dailyPrecips;

            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, "Exception while getting DarkSky data:");
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                trace.Flush();
                return new List<double>();
            }
        }

        public class sc_val
        {
            public int num;
            public double flow1;
            public double flow2;
            public double flow3;
            public double flow4;
            public double flow5;               
        }

        public class area_elev_val
        {
            public double elev;
            public double area;
        }

        public static double average(List<double> rain, List<int> periods, int period, int starting)
        {
            int length = periods[period];
            
            double sum_rain = 0;
            List<double> tempTest = new List<double>();
            for (int i = (starting-length); i < (starting); i++) // take the sum of the last (most recent) rain values (i.e. 7)
            { 
                sum_rain += rain[i];
                if (period ==1 )tempTest.Add(rain[i]);
            }
            double average_value = sum_rain/Convert.ToDouble(length);
            if (period == 1)
            {
                trace.TraceEvent(TraceEventType.Information, 1, "7-day precip values  = " + string.Join(",", tempTest));
                trace.Flush();
            }
            return average_value;
        }

        public class values
        {
            public double flow;
            public List<double> rain;
        }

        public static List<values> crearecords(List<double> flow, List<double> rain, List<int> periods)
        {
            int init = periods.Last(); 
            List<values> records = new List<values>();
            for (int i = 0; i < (rain.Count - init + 1); i++)  
            {
                values Value = new values();
                Value.flow = flow[init+i-1];  

                List<double> rain_temp = new List<double>();

                int starting = init+i;  
                for (int j = 0; j < periods.Count; j++)
                {
                    rain_temp.Add(average(rain, periods, j, starting)); 
                }
                Value.rain = rain_temp;
                records.Add(Value);
            }
            return records;
        }

        public static List<double> creatarget(List<double> forecast, int number, List<double> rain, List<int> periods)
        {
            List<double> rainextend = new List<double>();
            rainextend = rain.ToList();
            for (int i = 0; i < number; i++)    // number = 1 for "today", number = 2 for "tomorrow" etc... so only append one value for today
            {
                rainextend.Add(forecast[i]);    
            }

            int init = periods.Last(); 
            List<double> target = new List<double>();
            int starting = rainextend.Count();
            for (int j = 0; j < periods.Count; j++)
            {
                double value = average(rainextend, periods, j, starting);
                target.Add(value);
            }
            return target;  // list of period averages
        }

        public class maxmin_class
        {
            public double maxval;
            public double minval;
        }

        public static List<maxmin_class> calcmaxmin(List<values> records)
        {
            maxmin_class maxmin_var = new maxmin_class();
            List<maxmin_class> maxminlist = new List<maxmin_class>();
            
            List<double> flowList = new List<double>();
            foreach (values elem in records)
            {
                flowList.Add(elem.flow);
            }
            maxmin_var.minval = flowList.Min();
            maxmin_var.maxval = flowList.Max();
            maxminlist.Add(maxmin_var);

            int rainCount = records.First().rain.Count();
            for (int i = 0; i < rainCount; i++ )
            {
                maxmin_class maxmin_rain = new maxmin_class();
                List<double> rainList = new List<double>();

                foreach (values elem in records)
                {
                    rainList.Add(elem.rain[i]);
                }
                maxmin_rain.minval = rainList.Min();
                maxmin_rain.maxval = rainList.Max();

                rainList.Clear();
                maxminlist.Add(maxmin_rain);
            }

            return maxminlist;
        }

        public static double getdistance(List<double> target, List<double> scenario, List<maxmin_class> maxmin, double factor)
        {
            double dist = 0;
            for (int i = 0; i < target.Count; i++)
            {
                double maxim = maxmin[i+1].maxval;
                double minim = maxmin[i+1].minval;
                double tvalue = (target[i] - minim) / (maxim - minim);
                double svalue = factor * (scenario[i] - minim) / (maxim - minim);
                dist = dist + Math.Pow((tvalue - svalue),2.0);
            }
            return dist;
        }

        public class distelem_class
        {
            public double observed_flow;
            public double sel_flow;
            public double distance;
        }

        public static List<distelem_class> populatedistances(List<double> target, List<double> flow, List<double> rain, List<int> periods, int gap, List<double> multipliers, double power)
        {
            List<values> records = crearecords(flow, rain, periods);
            List<maxmin_class> maxmin = calcmaxmin(records);
            double obsflow = records.Last().flow;
            List<values> scenarios = new List<values>();
            int ss_upper = records.Count() - gap +1;
            for (int ss = 0; ss < ss_upper; ss++)
            {
                values values_temp = new values();
                values_temp.flow = records[ss].flow;
                values_temp.rain = records[ss].rain;
                scenarios.Add(values_temp);
            }
            List<distelem_class> distances = new List<distelem_class>();
            foreach(double factor in multipliers)
            {
                foreach(values elem in scenarios)
                {
                    List<double> scenario = elem.rain;
                    double dist = getdistance(target, scenario, maxmin, factor);
                    double hselflow = (Math.Pow(factor,power))*(elem.flow - maxmin.First().minval)/(maxmin.First().maxval - maxmin.First().minval);   
                    double selflow = maxmin.First().minval + hselflow*(maxmin.First().maxval - maxmin.First().minval);

                    distelem_class distelem = new distelem_class();
                    distelem.observed_flow = obsflow;
                    distelem.sel_flow = selflow;
                    distelem.distance = Math.Pow(dist,0.5);
                    distances.Add(distelem);
                }
            }
            return distances;
        }

        public static double calcIDW(List<double> target, List<double> flow, List<double> rain, List<int> periods, int gap, List<double> multipliers, double power, double IDWpower)
        {
            List<distelem_class> distances = populatedistances(target, flow, rain, periods, gap, multipliers, power);
            double numerator = 0;
            double denominator = 0;
            double value = new double();
            bool needinter = true;
            foreach (distelem_class dist in distances)
            {
                if (dist.distance == 0)
                {
                    needinter = false;
                    value = dist.sel_flow;
                }
                else
                {
                    numerator = numerator + dist.sel_flow / (Math.Pow(dist.distance,IDWpower));
                    denominator = denominator + 1 / (Math.Pow(dist.distance,IDWpower));
                }
            }
            if (needinter == true)
            {
                value = numerator / denominator;
            }
            return value;
        }

        public static double updatelevel(double Qin, double Qout, double oldlevel, List<area_elev_val> area_elev)
        {
            if (oldlevel < 0)
            {
                trace.TraceEvent(TraceEventType.Error, 1, "Level must be positive");
            }
            
            double deltaQ = Qin - Qout;
            double deltaVol = deltaQ * 60.0 * 60.0 * 24.0;
            double area = new double();
            foreach (area_elev_val area_elev_item in area_elev)
            {
                if (oldlevel >= area_elev_item.elev)  //TODO: maybe change to >= or set initial area to lowest in curve
                {
                    area = area_elev_item.area;
                }
            }
            double deltah = deltaVol / area;
            double newlevel = oldlevel + deltah;
            if (newlevel > 20.0)
            {
                trace.TraceEvent(TraceEventType.Error, 1, "Level above range");
            }
            if (newlevel < 0)
            {
                trace.TraceEvent(TraceEventType.Error, 1, "Level below range");
            }
            return newlevel;
        }

        public static double calcdamout(double dam_coef_a, double dam_coef_b, double dam_coef_c, double Qin_dam, double level_in)
        {
            double Qout_new = dam_coef_a + dam_coef_b * Math.Pow(Qin_dam, (dam_coef_c * level_in));
            return Qout_new;
        }

        public static sc_val findclosest(List<double> target, List<sc_val> scenarios)
        {
            double mindist = 0;
            int ck = 0;
            sc_val closest = new sc_val(); 

            foreach (sc_val sc in scenarios)
            {
                int number = sc.num;
                
                List<double> flows = new List<double> {sc.flow1, sc.flow2, sc.flow3, sc.flow4, sc.flow5};
                double dist = 0;
                for (int i=0; i< flows.Count(); i++)
                {
                    double difference = Math.Pow((target[i] - flows[i]),2.0);
                    dist = dist + difference; 
                }
                dist = Math.Pow(dist,0.5);
                if (ck == 0)
                {
                    mindist = dist;
                    ck = 1;
                }
                if (dist <= mindist)
                {
                    mindist = dist;
                    closest = sc;
                }
            }
            return closest;
        }
        public static bool SendEmail(string addresses, string subject, string msgBody)
        {
            bool success = false;

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("vlist@envmodeling.com", "c0rTad0s"),
                EnableSsl = true
            };

            MailMessage message = new MailMessage();
            message.From = new MailAddress("vlist@envmodeling.com");  //HARDWIRE: for demonstration only - replace with another email ("soporte@gmail.com")
            message.To.Add(addresses);
            message.Subject = subject;
            message.Body = msgBody;
            message.IsBodyHtml = false;
            message.Priority = MailPriority.Normal;

            try
            {
                client.Send(message);
                Console.WriteLine("Sent email");
                Console.ReadLine();
                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                trace.TraceEvent(TraceEventType.Error, 1, "Error sending email to " + addresses);
                trace.Flush();
            }

            return success;
        }

    }
}
